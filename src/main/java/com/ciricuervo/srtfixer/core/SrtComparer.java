/**
 * 
 */
package com.ciricuervo.srtfixer.core;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.List;
import java.util.OptionalDouble;

import org.apache.commons.lang3.StringUtils;

/**
 * @author ciri-cuervo
 */
public class SrtComparer {

	private static final double MIN_JARO_WINKLER_DISTANCE = 0.90;
	private static final long TIME_DIFFERENCE_OFFSET = 5000;
	private static final int TIME_DIFFERENCES_AMOUNT = 15;

	public static SrtTimeCorrections generateTimeCorrections(List<SrtSubtitle> subs1, List<SrtSubtitle> subs2, boolean matchFirst,
			boolean matchLast) {
		SrtTimeCorrections timeCorrections = new SrtTimeCorrections();

		if (subs1.isEmpty() || subs2.isEmpty()) {
			return timeCorrections;
		}

		SrtSubtitle sub1 = subs1.get(0);
		SrtSubtitle sub2 = subs2.get(0);
		long avgTimeDifference = sub2.getTimeFromMillis() - sub1.getTimeFromMillis();
		long minTimeDifference = avgTimeDifference - TIME_DIFFERENCE_OFFSET;
		long maxTimeDifference = avgTimeDifference + TIME_DIFFERENCE_OFFSET;
		Deque<Long> lastTimeDifferences = new ArrayDeque<>(TIME_DIFFERENCES_AMOUNT);

		int sub1Idx = 0;
		int lastMatch2 = -1;

		if (matchFirst) {
			timeCorrections.replace(sub1.getTimeFromMillis(), sub2.getTimeFromMillis(), sub1.getId());
			// update control variables
			avg(lastTimeDifferences, avgTimeDifference);
			lastMatch2++;
			sub1Idx++;
		}

		while (sub1Idx < subs1.size()) {
			sub1 = subs1.get(sub1Idx);

			int sub2Idx = lastMatch2 + 1;

			while (sub2Idx < subs2.size()) {
				sub2 = subs2.get(sub2Idx);

				long timeDiff = sub2.getTimeFromMillis() - sub1.getTimeFromMillis();
				if (timeDiff > maxTimeDifference) {
					// max difference exceeded, and it will continue increasing...
					break;
				}
				if (timeDiff < minTimeDifference) {
					// mix difference not reached...
					sub2Idx++;
					continue;
				}

				if (StringUtils.getJaroWinklerDistance(sub1.getSingleLine(), sub2.getSingleLine()) >= MIN_JARO_WINKLER_DISTANCE
						|| StringUtils.getJaroWinklerDistance(sub1.getInitDialogLine(), sub2.getInitDialogLine()) >= 0.99) {
					timeCorrections.replace(sub1.getTimeFromMillis(), sub2.getTimeFromMillis(), sub1.getId());
					lastMatch2 = sub2Idx;

					// update time differences
					avgTimeDifference = avg(lastTimeDifferences, timeDiff);
					minTimeDifference = avgTimeDifference - TIME_DIFFERENCE_OFFSET;
					maxTimeDifference = avgTimeDifference + TIME_DIFFERENCE_OFFSET;
					break;
				}

				sub2Idx++;
			}
			sub1Idx++;
		}

		if (matchLast) {
			sub1 = subs1.get(subs1.size() - 1);
			sub2 = subs2.get(subs2.size() - 1);
			timeCorrections.replace(sub1.getTimeFromMillis(), sub2.getTimeFromMillis(), sub1.getId());
		}

		return timeCorrections;
	}

	private static long avg(Deque<Long> lastTimeDifferences, Long timeDiff) {
		if (lastTimeDifferences.size() == TIME_DIFFERENCES_AMOUNT) {
			lastTimeDifferences.removeFirst();
		}
		lastTimeDifferences.addLast(timeDiff);
		OptionalDouble avg = lastTimeDifferences.stream().mapToLong(a -> a).average();
		return (long) (avg.getAsDouble() + 0.5); // rounded long
	}

}

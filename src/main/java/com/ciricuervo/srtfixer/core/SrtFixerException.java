/**
 * 
 */
package com.ciricuervo.srtfixer.core;

/**
 * @author ciri-cuervo
 */
public class SrtFixerException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public SrtFixerException() {}

	public SrtFixerException(String message) {
		super(message);
	}

	public SrtFixerException(Throwable cause) {
		super(cause);
	}

	public SrtFixerException(String message, Throwable cause) {
		super(message, cause);
	}

}

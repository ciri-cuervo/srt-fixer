/**
 * 
 */
package com.ciricuervo.srtfixer.core;

import java.time.DateTimeException;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.temporal.ChronoField;
import java.util.Locale;
import java.util.regex.Pattern;

import lombok.experimental.UtilityClass;

/**
 * @author ciri-cuervo
 */
@UtilityClass
public class SrtTimeFormatter {

	public static final Pattern SRT_LINE_PATTERN = Pattern.compile("\\d{2}:\\d{2}:\\d{2},\\d{3} --> \\d{2}:\\d{2}:\\d{2},\\d{3}");
	public static final String SRT_TIME_PATTERN = "HH:mm:ss,SSS";

	private static final DateTimeFormatter SRT_TIME_FORMATTER = DateTimeFormatter.ofPattern(SRT_TIME_PATTERN, Locale.ROOT);
	private static final long NANOS_IN_MILLI = 1000000;
	private static final String ZERO_TIME_TEXT = "00:00:00,000";

	/**
	 * 
	 * @param time
	 * @return
	 * @throws SrtFixerException
	 */
	public static String formatTime(long time) {
		try {
			return SRT_TIME_FORMATTER.format(LocalTime.ofNanoOfDay(time * NANOS_IN_MILLI));
		} catch (DateTimeException e) {
			throw new SrtFixerException("ERROR >> the value `" + time + "` is not correct", e);
		}
	}

	/**
	 * 
	 * @param text
	 * @return
	 * @throws SrtFixerException
	 */
	public static long parseTime(String text) {
		try {
			if (text.equals("-" + ZERO_TIME_TEXT)) {
				text = text.substring(1);
			}
			return SRT_TIME_FORMATTER.parse(text).get(ChronoField.MILLI_OF_DAY);
		} catch (DateTimeParseException e) {
			throw new SrtFixerException("ERROR >> the value `" + text + "` must respect the format 00:00:00,000", e);
		}
	}

}

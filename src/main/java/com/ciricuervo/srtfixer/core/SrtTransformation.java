/**
 * 
 */
package com.ciricuervo.srtfixer.core;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import com.ciricuervo.srtfixer.core.SrtTimeCorrections.SrtTimeCorrection;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author ciri-cuervo
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class SrtTransformation {

	private long timeFrom;
	private long timeTo;
	private double factor;
	private double offset;

	/**
	 * 
	 * @param time
	 * @return
	 */
	public boolean isInstantBetween(long time) {
		return time >= timeFrom && time <= timeTo;
	}

	@Override
	public String toString() {
		return String.format("%d --> %d | factor: %.2f | offset: %.2f", timeFrom, timeTo, factor, offset);
	}

	/*
	 * STATIC METHODS
	 */

	/**
	 * 
	 * @param transformations
	 * @param time
	 * @return
	 */
	public static SrtTransformation getTransformationForTime(List<SrtTransformation> transformations, long time) {
		return transformations.stream().filter(t -> t.isInstantBetween(time)).findFirst().orElse(null);
	}

	/**
	 * 
	 * @param transformations
	 * @param time
	 * @return
	 */
	public static long transformTime(List<SrtTransformation> transformations, long time) {
		SrtTransformation transformation = getTransformationForTime(transformations, time);

		if (transformation == null) {
			return time;
		}

		return transformTime(time, transformation.getFactor(), transformation.getOffset());
	}

	/**
	 * 
	 * @param time
	 * @param factor
	 * @param offset
	 * @return
	 */
	private static long transformTime(long time, double factor, double offset) {
		return (long) (time * factor + offset + 0.5d); // rounded long
	}

	/**
	 * 
	 * @param timeCorrections
	 * @param enclosed
	 * @return
	 */
	public static List<SrtTransformation> buildTransformations(SrtTimeCorrections timeCorrections, boolean enclosed) {
		assertValidTimeCorrections(timeCorrections);

		if (timeCorrections.size() == 0) {
			return Collections.emptyList();
		}

		// the set's iterator returns the entries in ascending key order.
		Iterator<SrtTimeCorrection> iterator = timeCorrections.iterator();
		SrtTimeCorrection prevTC = iterator.next();

		if (!iterator.hasNext()) {
			long t1 = prevTC.getFrom();
			long t2 = prevTC.getTo();
			double offset = t2 - t1;
			return Collections.singletonList(new SrtTransformation(0L, Long.MAX_VALUE, 1.0d, offset));
		}

		List<SrtTransformation> transformations = new ArrayList<>();

		while (iterator.hasNext()) {
			SrtTimeCorrection thisTC = iterator.next();
			long t1 = prevTC.getFrom();
			long t2 = prevTC.getTo();
			long t3 = thisTC.getFrom();
			long t4 = thisTC.getTo();
			// t1 can't equal t3 because they are map's keys
			// t2 can't equal t4 because of the check: #assertValidTimesMap()
			double factor = (double) (t4 - t2) / (t3 - t1);
			double offset = t2 - t1 * factor;

			transformations.add(new SrtTransformation(t1, t3, factor, offset));
			prevTC = thisTC;
		}

		if (!enclosed) {
			transformations.get(0).setTimeFrom(0L);
			transformations.get(transformations.size() - 1).setTimeTo(Long.MAX_VALUE);
		}

		return transformations;
	}

	/**
	 * 
	 * @param timeCorrections
	 * @throws SrtFixerException
	 */
	private static void assertValidTimeCorrections(SrtTimeCorrections timeCorrections) throws SrtFixerException {
		if (timeCorrections.size() == 0) {
			return;
		}

		// the set's iterator returns the entries in ascending key order.
		Iterator<SrtTimeCorrection> iterator = timeCorrections.iterator();
		SrtTimeCorrection prevTC = iterator.next();
		while (iterator.hasNext()) {
			SrtTimeCorrection thisTC = iterator.next();
			long prevTime = prevTC.getTo();
			long thisTime = thisTC.getTo();
			if (prevTime >= thisTime) {
				String prevText = SrtTimeFormatter.formatTime(prevTime);
				String thisText = SrtTimeFormatter.formatTime(thisTime);
				throw new SrtFixerException("subtitle id " + thisTC.getId() + ": " + thisText + " is lower or equal than " + prevText);
			}
			prevTC = thisTC;
		}
	}

	public static Long[] getPosibleTransformations(List<SrtTransformation> transformations, long time) {
		for (int i = 0; i < transformations.size(); i++) {
			SrtTransformation transformation = transformations.get(i);
			if (transformation.isInstantBetween(time)) {
				Long prevDiff = null;
				Long nextDiff = null;
				if (i > 0) {
					SrtTransformation prev = transformations.get(i - 1);
					prevDiff = transformTime(time, prev.getFactor(), prev.getOffset());
				}
				if (i < transformations.size() - 1) {
					SrtTransformation next = transformations.get(i + 1);
					nextDiff = transformTime(time, next.getFactor(), next.getOffset());
				}
				return new Long[] {prevDiff, nextDiff};
			}
		}
		return new Long[] {null, null};
	}

}

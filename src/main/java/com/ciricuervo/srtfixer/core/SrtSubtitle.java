/**
 * 
 */
package com.ciricuervo.srtfixer.core;

import static java.util.stream.Collectors.*;

import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author ciri-cuervo
 */
@NoArgsConstructor
public class SrtSubtitle {

	public static final long INVALID_MILLIS = Long.MIN_VALUE;

	private static final Pattern INIT_DIALOG_PATTERN = Pattern.compile("^- ?");

	@Getter
	@Setter
	private int id;

	private String timeFromText;

	private String timeToText;

	private long timeFromMillis = INVALID_MILLIS;

	private long timeToMillis = INVALID_MILLIS;

	private List<String> text;

	public String getTimeFromText() {
		if (timeFromText == null && timeFromMillis != INVALID_MILLIS) {
			timeFromText = SrtTimeFormatter.formatTime(timeFromMillis);
		}
		return timeFromText;
	}

	public void setTimeFromText(String timeFromText) {
		this.timeFromText = timeFromText;
		this.timeFromMillis = INVALID_MILLIS;
	}

	public String getTimeToText() {
		if (timeToText == null && timeToMillis != INVALID_MILLIS) {
			timeToText = SrtTimeFormatter.formatTime(timeToMillis);
		}
		return timeToText;
	}

	public void setTimeToText(String timeToText) {
		this.timeToText = timeToText;
		this.timeToMillis = INVALID_MILLIS;
	}

	public long getTimeFromMillis() {
		if (timeFromMillis == INVALID_MILLIS && timeFromText != null) {
			timeFromMillis = SrtTimeFormatter.parseTime(timeFromText);
		}
		return timeFromMillis;
	}

	public void setTimeFromMillis(long timeFromMillis) {
		this.timeFromMillis = timeFromMillis;
		this.timeFromText = null;
	}

	public long getTimeToMillis() {
		if (timeToMillis == INVALID_MILLIS && timeToText != null) {
			timeToMillis = SrtTimeFormatter.parseTime(timeToText);
		}
		return timeToMillis;
	}

	public void setTimeToMillis(long timeToMillis) {
		this.timeToMillis = timeToMillis;
		this.timeToText = null;
	}

	public List<String> getText() {
		return text;
	}

	public void setText(List<String> text) {
		this.text = text;
		this.textMultiLine = null;
		this.textSingleLine = null;
		this.initDialogLine = null;
	}

	private String textMultiLine;
	private String textSingleLine;
	private String initDialogLine;

	public String getMultiLine() {
		if (textMultiLine == null) {
			textMultiLine = String.join("\n", text);
		}
		return textMultiLine;
	}

	public String getSingleLine() {
		if (textSingleLine == null) {
			textSingleLine = text.stream().map(SrtSubtitle::cleanFormat).collect(joining(" "));
		}
		return textSingleLine;
	}

	public String getInitDialogLine() {
		if (initDialogLine == null) {
			if (isDialog(text)) {
				initDialogLine = cleanFormat(INIT_DIALOG_PATTERN.matcher(text.get(0)).replaceFirst(""));
			} else {
				initDialogLine = getSingleLine();
			}
		}
		return initDialogLine;
	}

	private static boolean isDialog(List<String> text) {
		return text.size() > 1 && text.stream().allMatch(t -> t.startsWith("-"));
	}

	private static String cleanFormat(String text) {
		return StringUtils.removeEnd(StringUtils.removeStart(text.toLowerCase(Locale.ROOT), "<i>"), "</i>");
	}

	@Override
	public String toString() {
		return String.format("%d\n%s --> %s\n%s", id, getTimeFromText(), getTimeToText(), getMultiLine());
	}

}

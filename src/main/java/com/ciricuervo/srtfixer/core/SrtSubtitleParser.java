/**
 * 
 */
package com.ciricuervo.srtfixer.core;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * @author ciri-cuervo
 */
public class SrtSubtitleParser {

	private static final Pattern SRT_ID_PATTERN = Pattern.compile("\\d+");

	private List<SrtSubtitle> subtitles = null;
	private SrtSubtitle subtitle = null;
	private List<String> text = new ArrayList<>(8);

	public List<SrtSubtitle> parseFile(List<String> lines) throws SrtFixerException {
		subtitles = new ArrayList<>(lines.size() / 4);

		for (int i = -1; i < lines.size(); i++) {
			if (i < 0 || "".equals(lines.get(i))) {
				if (i + 1 < lines.size()) {
					int lastId = subtitle == null ? 0 : subtitle.getId();
					if (matchesNextId(lines.get(i + 1), lastId)) {

						addCurrentSubtitle();

						subtitle = new SrtSubtitle();
						subtitle.setId(Integer.parseInt(lines.get(i + 1)));

						if (i + 2 < lines.size()) {

							if (!SrtTimeFormatter.SRT_LINE_PATTERN.matcher(lines.get(i + 2)).matches()) {
								throw new SrtFixerException("line " + (i + 2) + " doesn't have the expected time format.");
							}

							subtitle.setTimeFromText(lines.get(i + 2).substring(0, 12));
							subtitle.setTimeToText(lines.get(i + 2).substring(17, 29));
							i = i + 2;
						} else {
							i = i + 1;
						}

					} else {
						if (i >= 0) {
							text.add(lines.get(i).trim());
						}
					}
				} else {
					// no action
				}
			} else {
				text.add(lines.get(i).trim());
			}
		}

		addCurrentSubtitle();

		return subtitles;
	}

	private void addCurrentSubtitle() {
		if (subtitle != null) {
			subtitle.setText(new ArrayList<>(text));
			subtitles.add(subtitle);

			subtitle = null;
			text.clear();
		}
	}

	private static boolean matchesNextId(String nextId, int lastId) {
		return SRT_ID_PATTERN.matcher(nextId).matches() && Integer.parseInt(nextId) == lastId + 1;
	}

	public static List<String> formatFile(List<SrtSubtitleCorrection> corrections) throws SrtFixerException {
		List<String> lines = new ArrayList<>(corrections.size() * 6);
		corrections.forEach(correction -> {
			if (correction.getTimeFromMillis() < 0L) {
				throw new SrtFixerException("the subtitle " + correction.getSubtitle().getId() + " has negative times.");
			}
			if (lines.size() > 0) {
				lines.add("");
			}
			lines.add(correction.toString());
		});
		return lines;
	}

}

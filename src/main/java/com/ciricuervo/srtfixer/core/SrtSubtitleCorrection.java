/**
 * 
 */
package com.ciricuervo.srtfixer.core;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;

/**
 * @author ciri-cuervo
 */
@NoArgsConstructor
public class SrtSubtitleCorrection {

	@Getter
	@Setter
	@NonNull
	private SrtSubtitle subtitle;

	private String timeFromText;

	private String timeToText;

	private long timeFromMillis = SrtSubtitle.INVALID_MILLIS;

	private long timeToMillis = SrtSubtitle.INVALID_MILLIS;

	@Getter
	@Setter
	private boolean warning;

	public SrtSubtitleCorrection(SrtSubtitle subtitle) {
		this.subtitle = subtitle;
	}

	public String getTimeFromText() {
		if (timeFromText == null) {
			if (timeFromMillis != SrtSubtitle.INVALID_MILLIS) {
				timeFromText = negativePrefix(timeFromMillis) + SrtTimeFormatter.formatTime(Math.abs(timeFromMillis));
			} else {
				timeFromText = subtitle.getTimeFromText();
			}
		}
		return timeFromText;
	}

	public void setTimeFromText(String timeFromText) {
		this.timeFromText = timeFromText;
		this.timeFromMillis = SrtSubtitle.INVALID_MILLIS;
	}

	public String getTimeToText() {
		if (timeToText == null) {
			if (timeToMillis != SrtSubtitle.INVALID_MILLIS) {
				timeToText = negativePrefix(timeToMillis) + SrtTimeFormatter.formatTime(Math.abs(timeToMillis));
			} else {
				timeToText = subtitle.getTimeToText();
			}
		}
		return timeToText;
	}

	public void setTimeToText(String timeToText) {
		this.timeToText = timeToText;
		this.timeToMillis = SrtSubtitle.INVALID_MILLIS;
	}

	public long getTimeFromMillis() {
		if (timeFromMillis == SrtSubtitle.INVALID_MILLIS) {
			if (timeFromText != null) {
				timeFromMillis = SrtTimeFormatter.parseTime(timeFromText);
			} else {
				timeFromMillis = subtitle.getTimeFromMillis();
			}
		}
		return timeFromMillis;
	}

	public void setTimeFromMillis(long timeFromMillis) {
		this.timeFromMillis = timeFromMillis;
		this.timeFromText = null;
	}

	public long getTimeToMillis() {
		if (timeToMillis == SrtSubtitle.INVALID_MILLIS) {
			if (timeToText != null) {
				timeToMillis = SrtTimeFormatter.parseTime(timeToText);
			} else {
				timeToMillis = subtitle.getTimeToMillis();
			}
		}
		return timeToMillis;
	}

	public void setTimeToMillis(long timeToMillis) {
		this.timeToMillis = timeToMillis;
		this.timeToText = null;
	}

	public void clear() {
		timeFromText = null;
		timeToText = null;
		timeFromMillis = SrtSubtitle.INVALID_MILLIS;
		timeToMillis = SrtSubtitle.INVALID_MILLIS;
		warning = false;
	}

	@Override
	public String toString() {
		return String.format("%d\n%s --> %s\n%s", subtitle.getId(), getTimeFromText(), getTimeToText(), subtitle.getMultiLine());
	}

	private static String negativePrefix(long time) {
		return time < 0L ? "-" : "";
	}

}

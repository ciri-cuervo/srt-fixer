/**
 * 
 */
package com.ciricuervo.srtfixer.core;

import java.util.TreeSet;

import com.ciricuervo.srtfixer.core.SrtTimeCorrections.SrtTimeCorrection;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

/**
 * @author ciri-cuervo
 */
@NoArgsConstructor
public class SrtTimeCorrections extends TreeSet<SrtTimeCorrection> {

	private static final long serialVersionUID = -5499618919522530000L;

	@Data
	@EqualsAndHashCode(of = "from")
	@RequiredArgsConstructor
	@AllArgsConstructor
	public static class SrtTimeCorrection implements Comparable<SrtTimeCorrection> {

		private final long from;
		private final long to;
		private int id;

		public SrtTimeCorrection(long from) {
			this.from = from;
			this.to = 0L;
		}

		@Override
		public int compareTo(SrtTimeCorrection tc) {
			return Long.compare(this.from, tc.from);
		}
	}

	public SrtTimeCorrections(SrtTimeCorrections timeCorrections) {
		super(timeCorrections);
	}

	public boolean add(long from, long to) {
		return super.add(new SrtTimeCorrection(from, to));
	}

	public boolean add(long from, long to, int id) {
		return super.add(new SrtTimeCorrection(from, to, id));
	}

	public boolean replace(long from, long to) {
		return this.replace(from, to, 0);
	}

	public boolean replace(long from, long to, int id) {
		SrtTimeCorrection timeCorrection = new SrtTimeCorrection(from, to, id);
		super.remove(timeCorrection);
		return super.add(timeCorrection);
	}

	public boolean addAll(SrtTimeCorrections timeCorrections) {
		return super.addAll(timeCorrections);
	}

	public boolean remove(long from) {
		return super.remove(new SrtTimeCorrection(from));
	}

	public boolean contains(long from) {
		return super.contains(new SrtTimeCorrection(from));
	}

}

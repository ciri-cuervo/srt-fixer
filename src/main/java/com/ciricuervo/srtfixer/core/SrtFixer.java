/**
 * 
 */
package com.ciricuervo.srtfixer.core;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

/**
 * @author ciri-cuervo
 */
public class SrtFixer {

	/**
	 * 
	 * @param corrections
	 * @param timeCorrections
	 * @param enclosed
	 * @throws SrtFixerException
	 */
	public static void performCorrections(List<SrtSubtitleCorrection> corrections, SrtTimeCorrections timeCorrections, boolean enclosed)
			throws SrtFixerException {

		List<SrtTransformation> transformations = SrtTransformation.buildTransformations(timeCorrections, enclosed);

		if (transformations.size() == 0) {
			corrections.forEach(SrtSubtitleCorrection::clear);
			return;
		}

		List<Long[]> newValuesList = new ArrayList<>(corrections.size());

		for (SrtSubtitleCorrection correction : corrections) {
			SrtSubtitle subtitle = correction.getSubtitle();

			long timeFrom = SrtTransformation.transformTime(transformations, subtitle.getTimeFromMillis());
			long timeTo = SrtTransformation.transformTime(transformations, subtitle.getTimeToMillis());

			if (newValuesList.size() > 0) {
				Long[] prevValues = newValuesList.get(newValuesList.size() - 1);
				if (timeFrom <= prevValues[0] || timeTo <= prevValues[1]) {
					throw new SrtFixerException("subtitle " + subtitle.getId() + ", new value is lower or equal than previous subtitle");
				}
			}

			newValuesList.add(new Long[] {timeFrom, timeTo});
		}

		/* at this point all corrections were calculated successfully */

		for (int i = 0; i < corrections.size(); i++) {
			SrtSubtitleCorrection correction = corrections.get(i);
			Long[] newValues = newValuesList.get(i);
			correction.setTimeFromMillis(newValues[0]);
			correction.setTimeToMillis(newValues[1]);
			correction.setWarning(false);
		}

		for (SrtSubtitleCorrection correction : corrections) {
			SrtSubtitle subtitle = correction.getSubtitle();

			if (timeCorrections.contains(subtitle.getTimeFromMillis())) {
				continue;
			}

			Long[] posFrom = SrtTransformation.getPosibleTransformations(transformations, subtitle.getTimeFromMillis());
			Long[] posTo = SrtTransformation.getPosibleTransformations(transformations, subtitle.getTimeToMillis());

			long diffFrom = 0;
			long diffTo = 0;

			if (posFrom[0] != null) {
				diffFrom = Math.max(diffFrom, Math.abs(posFrom[0] - correction.getTimeFromMillis()));
			}
			if (posFrom[1] != null) {
				diffFrom = Math.max(diffFrom, Math.abs(posFrom[1] - correction.getTimeFromMillis()));
			}
			if (posTo[0] != null) {
				diffTo = Math.max(diffTo, Math.abs(posTo[0] - correction.getTimeToMillis()));
			}
			if (posTo[1] != null) {
				diffTo = Math.max(diffTo, Math.abs(posTo[1] - correction.getTimeToMillis()));
			}

			if (diffFrom > 7500L || diffTo > 7500L) {
				correction.setWarning(true);
			}
		}
	}

	/**
	 * 
	 * @param path
	 * @param cs
	 * @return
	 * @throws SrtFixerException
	 */
	public static List<String> loadFile(Path path, Charset cs) throws SrtFixerException {
		try {
			return Files.readAllLines(path, cs);
		} catch (IOException e) {
			throw new SrtFixerException("Error reading file: " + path, e);
		}
	}

	/**
	 * 
	 * @param path
	 * @param lines
	 * @param cs
	 * @throws SrtFixerException
	 */
	public static void saveFile(Path path, List<String> lines, Charset cs) throws SrtFixerException {
		try {
			Files.write(path, lines, cs);
		} catch (IOException e) {
			throw new SrtFixerException("Error writing file: " + path, e);
		}
	}

}

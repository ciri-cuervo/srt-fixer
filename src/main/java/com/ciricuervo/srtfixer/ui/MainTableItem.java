/**
 * 
 */
package com.ciricuervo.srtfixer.ui;

import com.ciricuervo.srtfixer.core.SrtSubtitleCorrection;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * @author ciri-cuervo
 */
public class MainTableItem {

	private final StringProperty time;
	private final StringProperty correction;
	private final IntegerProperty id;
	private final StringProperty text;

	@Getter
	@Setter
	private SrtSubtitleCorrection subtitleCorrection;

	public MainTableItem(SrtSubtitleCorrection subtitleCorrection) {
		this.time = new SimpleStringProperty(subtitleCorrection.getSubtitle().getTimeFromText());
		this.correction = new SimpleStringProperty(subtitleCorrection.getTimeFromText());
		this.id = new SimpleIntegerProperty(subtitleCorrection.getSubtitle().getId());
		this.text = new SimpleStringProperty(subtitleCorrection.getSubtitle().getMultiLine());
		this.subtitleCorrection = subtitleCorrection;
	}

	public String getTime() {
		return time.get();
	}

	public void setTime(String time) {
		this.time.set(time);
	}

	public StringProperty timeProperty() {
		return time;
	}

	public String getCorrection() {
		return correction.get();
	}

	public void setCorrection(String correction) {
		this.correction.set(correction);
	}

	public StringProperty correctionProperty() {
		return correction;
	}

	public int getId() {
		return id.get();
	}

	public void setId(int id) {
		this.id.set(id);
	}

	public IntegerProperty idProperty() {
		return id;
	}

	public String getText() {
		return text.get();
	}

	public void setText(String text) {
		this.text.set(text);
	}

	public StringProperty textProperty() {
		return text;
	}

}

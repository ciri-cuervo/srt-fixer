/**
 * 
 */
package com.ciricuervo.srtfixer.ui;

import static java.util.stream.Collectors.*;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import com.ciricuervo.srtfixer.core.SrtComparer;
import com.ciricuervo.srtfixer.core.SrtFixer;
import com.ciricuervo.srtfixer.core.SrtFixerException;
import com.ciricuervo.srtfixer.core.SrtSubtitle;
import com.ciricuervo.srtfixer.core.SrtSubtitleCorrection;
import com.ciricuervo.srtfixer.core.SrtSubtitleParser;
import com.ciricuervo.srtfixer.core.SrtTimeCorrections;
import com.ciricuervo.srtfixer.core.SrtTimeFormatter;

import javafx.beans.Observable;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.css.PseudoClass;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.util.converter.NumberStringConverter;
import lombok.extern.java.Log;

/**
 * @author ciri-cuervo
 */
@Log
public class MainController {

	@FXML
	private TableView<MainTableItem> table;
	@FXML
	private Button btnOpenSrt;
	@FXML
	private Button btnSaveSrt;
	@FXML
	private ToggleButton btnOnlyEnclosed;
	@FXML
	private Button btnSynchronize;
	@FXML
	private ToggleButton btnMatchFirst;
	@FXML
	private ToggleButton btnMatchLast;
	@FXML
	private Label lblWorkingOn;
	@FXML
	private Label lblWorkingSrt;
	@FXML
	private Pane drawingPane;

	private FileChooser fileChooser;

	private final List<SrtSubtitleCorrection> corrections = new ArrayList<>();
	private final SrtTimeCorrections timeCorrections = new SrtTimeCorrections();
	private final List<SrtSubtitle> synchro = new ArrayList<>();

	@FXML
	public void initialize() {
		fileChooser = new FileChooser();
		File location = new File(getClass().getProtectionDomain().getCodeSource().getLocation().getPath());
		fileChooser.setInitialDirectory(location);
		fileChooser.getExtensionFilters().addAll(new ExtensionFilter("SRT Files", "*.srt"));

		TableColumn<MainTableItem, String> timeColumn = new TableColumn<>("Time");
		timeColumn.setCellValueFactory(cellData -> cellData.getValue().timeProperty());
		timeColumn.setCellFactory(TextFieldTableCell.forTableColumn());
		timeColumn.setEditable(false);
		timeColumn.setSortable(false);
		timeColumn.setResizable(false);
		timeColumn.setMinWidth(150.0d);
		timeColumn.setPrefWidth(150.0d);
		timeColumn.setMaxWidth(150.0d);

		TableColumn<MainTableItem, String> correctionColumn = new TableColumn<>("Correction");
		correctionColumn.setCellValueFactory(cellData -> cellData.getValue().correctionProperty());
		correctionColumn.setCellFactory(TextFieldTableCell.forTableColumn());
		correctionColumn.setEditable(true);
		correctionColumn.setSortable(false);
		correctionColumn.setResizable(false);
		correctionColumn.setMinWidth(150.0d);
		correctionColumn.setPrefWidth(150.0d);
		correctionColumn.setMaxWidth(150.0d);

		TableColumn<MainTableItem, Number> idColumn = new TableColumn<>("Id");
		idColumn.setCellValueFactory(cellData -> cellData.getValue().idProperty());
		idColumn.setCellFactory(TextFieldTableCell.forTableColumn(new NumberStringConverter()));
		idColumn.setEditable(false);
		idColumn.setSortable(false);
		idColumn.setResizable(false);
		idColumn.setMinWidth(150.0d);
		idColumn.setPrefWidth(150.0d);
		idColumn.setMaxWidth(150.0d);

		TableColumn<MainTableItem, String> textColumn = new TableColumn<>("Text");
		textColumn.setCellValueFactory(cellData -> cellData.getValue().textProperty());
		textColumn.setCellFactory(TextFieldTableCell.forTableColumn());
		textColumn.setEditable(false);
		textColumn.setSortable(false);
		textColumn.setResizable(true);
		textColumn.setMinWidth(400.0d);
		textColumn.setPrefWidth(500.0d);
		textColumn.setMaxWidth(500.0d);

		PseudoClass timeCorrectionClass = PseudoClass.getPseudoClass("time-correction");
		PseudoClass timeNegativeClass = PseudoClass.getPseudoClass("time-negative");
		PseudoClass timeNotChangedClass = PseudoClass.getPseudoClass("time-notchanged");
		PseudoClass timeWarningClass = PseudoClass.getPseudoClass("time-warning");
		table.setRowFactory(tableView -> new TableRow<MainTableItem>() {

			@Override
			public void updateItem(MainTableItem item, boolean empty) {
				super.updateItem(item, empty);
				SrtSubtitleCorrection sc = empty ? null : item.getSubtitleCorrection();
				pseudoClassStateChanged(timeCorrectionClass, !empty && timeCorrections.contains(sc.getSubtitle().getTimeFromMillis()));
				pseudoClassStateChanged(timeNegativeClass, !empty && item.getCorrection().startsWith("-"));
				pseudoClassStateChanged(timeNotChangedClass, !empty && item.getCorrection().equals(item.getTime()));
				pseudoClassStateChanged(timeWarningClass, !empty && sc.isWarning());
			}
		});

		table.getColumns().add(timeColumn);
		table.getColumns().add(correctionColumn);
		table.getColumns().add(idColumn);
		table.getColumns().add(textColumn);

		ObservableList<MainTableItem> items = FXCollections.observableArrayList(
				item -> new Observable[] {item.timeProperty(), item.correctionProperty(), item.idProperty(), item.textProperty()});

		table.setItems(items);
		table.getItems().addListener((ListChangeListener<MainTableItem>) (change -> {
			while (change.next()) {
				if (change.wasUpdated()) {
					MainTableItem item = change.getList().get(change.getFrom());
					String time = item.getTime();
					String correction = item.getCorrection();
					int id = item.getId();

					SrtTimeCorrections tempTimeCorrections = new SrtTimeCorrections(timeCorrections);

					try {
						if ("".equals(correction)) {
							long timeMillis = SrtTimeFormatter.parseTime(time);
							tempTimeCorrections.remove(timeMillis);
						} else {
							long timeMillis = SrtTimeFormatter.parseTime(time);
							long correctionMillis = SrtTimeFormatter.parseTime(correction);
							tempTimeCorrections.replace(timeMillis, correctionMillis, id);
						}

						SrtFixer.performCorrections(corrections, tempTimeCorrections, btnOnlyEnclosed.isSelected());

						refreshUI(tempTimeCorrections);

					} catch (SrtFixerException e) {
						log.log(Level.SEVERE, "SRT Fixer error", e);

						// restore previous value
						table.getItems().set(change.getFrom(), new MainTableItem(item.getSubtitleCorrection()));
					}
				}
			}
		}));
	}

	private void addCircle(int id) {
		Circle circle = new Circle();
		double radius = drawingPane.getWidth() / 4.0;
		double minX = drawingPane.getWidth() / 2.0;
		double minY = minX + 12.0; // 12 is the height of the arrows
		double maxY = drawingPane.getHeight() - minY;
		circle.setRadius(radius);
		circle.setLayoutX(minX);
		circle.setLayoutY(minY + (maxY - minY) * (corrections.size() == 1 ? 0.5 : (double) (id - 1) / (corrections.size() - 1)));
		circle.setStrokeWidth(0.0);
		circle.setFill(Color.DARKSLATEGRAY.deriveColor(1.0, 1.0, 1.0, 0.5));
		drawingPane.getChildren().add(circle);
	}

	@FXML
	public void handleOpenSrt() {
		Path srtFilePath = pickFile();
		if (srtFilePath == null) {
			return;
		}

		try {
			List<String> lines = SrtFixer.loadFile(srtFilePath, StandardCharsets.ISO_8859_1);
			List<SrtSubtitle> subtitles = new SrtSubtitleParser().parseFile(lines);

			/* if parsing was correct.. */

			btnSaveSrt.setDisable(false);
			btnSynchronize.setDisable(false);
			lblWorkingOn.setText("Working on:");
			lblWorkingSrt.setText(srtFilePath.getFileName().toString());

			corrections.clear();
			corrections.addAll(subtitles.stream().map(SrtSubtitleCorrection::new).collect(toList()));

			synchro.clear();
			btnMatchFirst.setDisable(true);
			btnMatchLast.setDisable(true);

			refreshUI(null);

		} catch (SrtFixerException e) {
			log.log(Level.SEVERE, "SRT Fixer error", e);
		}
	}

	@FXML
	public void handleSaveSrt() {
		Path srtFilePath = saveFile();
		if (srtFilePath == null) {
			return;
		}

		try {
			List<String> lines = SrtSubtitleParser.formatFile(corrections);
			SrtFixer.saveFile(srtFilePath, lines, StandardCharsets.ISO_8859_1);
		} catch (SrtFixerException e) {
			log.log(Level.SEVERE, "SRT Fixer error", e);
		}
	}

	@FXML
	public void handleOnlyEnclosed() {
		boolean onlyEnclosed = btnOnlyEnclosed.selectedProperty().get();

		try {
			SrtFixer.performCorrections(corrections, timeCorrections, onlyEnclosed);

			table.getItems().clear();
			table.getItems().addAll(corrections.stream().map(MainTableItem::new).collect(toList()));

		} catch (SrtFixerException e) {
			log.log(Level.SEVERE, "SRT Fixer error", e);

			btnOnlyEnclosed.selectedProperty().set(!onlyEnclosed);
		}
	}

	@FXML
	public void handleSynchronize() {
		Path srtFilePath = pickFile();
		if (srtFilePath == null) {
			return;
		}

		try {
			List<String> lines = SrtFixer.loadFile(srtFilePath, StandardCharsets.ISO_8859_1);
			List<SrtSubtitle> subtitles = new SrtSubtitleParser().parseFile(lines);

			/* if parsing was correct.. */

			synchro.clear();
			synchro.addAll(subtitles);

			btnMatchFirst.setDisable(false);
			btnMatchFirst.setSelected(false);
			btnMatchLast.setDisable(false);
			btnMatchLast.setSelected(false);

			performSynchroTimeCorrections();

		} catch (SrtFixerException e) {
			log.log(Level.SEVERE, "SRT Fixer error", e);
		}
	}

	@FXML
	public void handleMatchFirst() {
		boolean matchFirst = btnMatchFirst.selectedProperty().get();

		try {
			performSynchroTimeCorrections();

		} catch (SrtFixerException e) {
			log.log(Level.SEVERE, "SRT Fixer error", e);

			btnMatchFirst.selectedProperty().set(!matchFirst);
		}
	}

	@FXML
	public void handleMatchLast() {
		boolean matchLast = btnMatchLast.selectedProperty().get();

		try {
			performSynchroTimeCorrections();

		} catch (SrtFixerException e) {
			log.log(Level.SEVERE, "SRT Fixer error", e);

			btnMatchLast.selectedProperty().set(!matchLast);
		}
	}

	private void performSynchroTimeCorrections() {
		boolean matchFirst = btnMatchFirst.selectedProperty().get();
		boolean matchLast = btnMatchLast.selectedProperty().get();

		List<SrtSubtitle> toSynch = corrections.stream().map(SrtSubtitleCorrection::getSubtitle).collect(toList());
		SrtTimeCorrections tempTimeCorrections = SrtComparer.generateTimeCorrections(toSynch, synchro, matchFirst, matchLast);

		SrtFixer.performCorrections(corrections, tempTimeCorrections, false);

		refreshUI(tempTimeCorrections);

		if (btnOnlyEnclosed.selectedProperty().get()) {
			handleOnlyEnclosed();
		}
	}

	private void refreshUI(SrtTimeCorrections tempTimeCorrections) {
		table.getItems().clear();
		timeCorrections.clear();
		drawingPane.getChildren().clear();

		table.getItems().addAll(corrections.stream().map(MainTableItem::new).collect(toList()));

		if (tempTimeCorrections != null) {
			timeCorrections.addAll(tempTimeCorrections);
			timeCorrections.stream().map(tc -> tc.getId()).distinct().forEach(this::addCircle);
		}

		btnOnlyEnclosed.setDisable(timeCorrections.size() < 2);
	}

	private Path pickFile() {
		fileChooser.setTitle("Open SRT File");
		File selectedFile = fileChooser.showOpenDialog(table.getScene().getWindow());
		if (selectedFile != null) {
			fileChooser.setInitialDirectory(selectedFile.getParentFile());
			return Paths.get(selectedFile.getPath());
		}
		return null;
	}

	private Path saveFile() {
		fileChooser.setTitle("Save SRT File");
		File selectedFile = fileChooser.showSaveDialog(table.getScene().getWindow());
		if (selectedFile != null) {
			fileChooser.setInitialDirectory(selectedFile.getParentFile());
			return Paths.get(selectedFile.getPath());
		}
		return null;
	}

}

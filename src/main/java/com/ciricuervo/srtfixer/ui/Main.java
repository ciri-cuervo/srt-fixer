/**
 * 
 */
package com.ciricuervo.srtfixer.ui;

import java.util.logging.Level;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import lombok.extern.java.Log;

/**
 * @author ciri-cuervo
 */
@Log
public class Main extends Application {

	@Override
	public void start(Stage primaryStage) {
		try {
			AnchorPane root = (AnchorPane) FXMLLoader.load(getClass().getResource("/main.fxml"));
			Scene scene = new Scene(root, 1024, 768);
			if (getClass().getResource("/main.css") != null) {
				scene.getStylesheets().add(getClass().getResource("/main.css").toExternalForm());
			}
			primaryStage.setTitle("SRT Fixer v1.0 beta 5");
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch (Throwable t) {
			log.log(Level.SEVERE, "Error when starting the GUI!", t);
			Platform.exit();
		}
	}

	public static void main(String[] args) {
		try {
			launch(args);
		} catch (Throwable t) {
			log.log(Level.SEVERE, "Error when lauching the APP!", t);
		} finally {
			System.exit(0);
		}
	}

}

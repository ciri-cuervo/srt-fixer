/**
 * 
 */
package com.ciricuervo.srtfixer.core;

import static org.junit.Assert.*;

import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * @author Ciri
 *
 */
public class SrtSubtitleParserTests {

	private SrtSubtitleParser parser;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		System.out.println("BEGIN TEST CASE");
		parser = new SrtSubtitleParser();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		System.out.println("END TEST CASE");
	}

	@Test
	public void testCase1() throws SrtFixerException {
		Path testCaseFile = Paths.get("./src/test/resources/srt", "testcase1.srt");
		List<String> srtFile = SrtFixer.loadFile(testCaseFile, StandardCharsets.ISO_8859_1);
		srtFile.forEach(System.out::println);
		List<SrtSubtitle> subtitles = parser.parseFile(srtFile);
		subtitles.forEach(System.out::println);
		assertEquals(3, subtitles.size());
	}

	@Test
	public void testCase2() throws SrtFixerException {
		Path testCaseFile = Paths.get("./src/test/resources/srt", "testcase2.srt");
		List<String> srtFile = SrtFixer.loadFile(testCaseFile, StandardCharsets.ISO_8859_1);
		srtFile.forEach(System.out::println);
		List<SrtSubtitle> subtitles = parser.parseFile(srtFile);
		subtitles.forEach(System.out::println);
		assertEquals(3, subtitles.size());
	}

	@Test
	public void testCase3() throws SrtFixerException {
		Path testCaseFile = Paths.get("./src/test/resources/srt", "testcase3.srt");
		List<String> srtFile = SrtFixer.loadFile(testCaseFile, StandardCharsets.ISO_8859_1);
		srtFile.forEach(System.out::println);
		List<SrtSubtitle> subtitles = parser.parseFile(srtFile);
		subtitles.forEach(System.out::println);
		assertEquals(3, subtitles.size());
	}

}

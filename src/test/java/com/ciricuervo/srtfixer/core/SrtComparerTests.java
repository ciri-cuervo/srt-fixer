/**
 * 
 */
package com.ciricuervo.srtfixer.core;

import static java.util.stream.Collectors.*;

import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * @author Ciri
 *
 */
public class SrtComparerTests {

	private SrtSubtitleParser parser;

	/**
	 * @throws Exception
	 */
	@Before
	public void setUp() throws Exception {
		System.out.println("BEGIN TEST CASE");
		parser = new SrtSubtitleParser();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		System.out.println("END TEST CASE");
		System.out.println();
	}

	@Test
	public void testCase() throws SrtFixerException {
		Path testCaseFile1 = Paths.get("./src/test/resources/srt", "test-compare-sub1.srt");
		Path testCaseFile2 = Paths.get("./src/test/resources/srt", "test-compare-sub2.srt");
		List<String> srtFile1 = SrtFixer.loadFile(testCaseFile1, StandardCharsets.ISO_8859_1);
		List<String> srtFile2 = SrtFixer.loadFile(testCaseFile2, StandardCharsets.ISO_8859_1);
		List<SrtSubtitle> subtitles1 = parser.parseFile(srtFile1);
		List<SrtSubtitle> subtitles2 = parser.parseFile(srtFile2);

		SrtTimeCorrections timeCorrections = SrtComparer.generateTimeCorrections(subtitles1, subtitles2, true, false);
		List<SrtSubtitleCorrection> corrections = subtitles1.stream().map(SrtSubtitleCorrection::new).collect(toList());
		SrtFixer.performCorrections(corrections, timeCorrections, false);
		corrections.get(0).setTimeFromMillis(0);
		Path testCaseResult = Paths.get("./target", "test-compare-result.srt");
		List<String> lines = SrtSubtitleParser.formatFile(corrections);
		SrtFixer.saveFile(testCaseResult, lines, StandardCharsets.ISO_8859_1);
	}

}
